# Scraping Web Content
Scraping web content with Python using Scrapy and Splinter.

# Getting Proxy List
[sslproxies.py](https://github.com/saisyam/web-scraping/blob/master/sslproxies.py) - Scrape SSL proxies from https://sslproxies.org to get the list of free SSL Proxies to try out for experimental purposes. For commercial use, you can purchase the proxy list from [https://www.sslproxies.org/#pricing](https://www.sslproxies.org/#pricing)

```shell
$ python3 sslproxies.py > proxies.txt
```
