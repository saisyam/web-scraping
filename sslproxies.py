# Scraping SSL Proxies to get the list of proxies
# This program will give the list of proxies
# in the format required for Scrapy

from splinter import Browser
import random
import time

def get_proxies(browser):
    table = browser.find_by_id("proxylisttable")
    tbody = table.find_by_tag('tbody')
    rows = tbody.find_by_tag("tr")
    for row in rows:
        tds = row.find_by_tag("td")
        proxy = tds[0].text+":"+tds[1].text
        print(proxy)

browser = Browser(headless=True)
browser.visit("https://www.sslproxies.org/")
get_proxies(browser)

next = browser.find_by_id('proxylisttable_next').first
while next.has_class("disabled") == False:
    atag = next.find_by_tag('a')
    atag.click()
    random.seed(random.randrange(1, 50, 2))
    time.sleep(random.randrange(1, 10, 1))
    get_proxies(browser)
    next = browser.find_by_id('proxylisttable_next').first
